import { defineConfig } from 'astro/config';

import preact from "@astrojs/preact";

// https://astro.build/config
export default defineConfig({
  site: 'https://quiet-pegasus-9b016e.netlify.app/',
  integrations: [preact()]
});